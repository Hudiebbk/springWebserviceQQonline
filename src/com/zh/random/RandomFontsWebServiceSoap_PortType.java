/**
 * RandomFontsWebServiceSoap_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.zh.random;

public interface RandomFontsWebServiceSoap_PortType extends java.rmi.Remote {

    /**
     * <br /><h3>获得随机中文简体字Web Services</h3><p>输入参数：byFontsLength =
     * 输出中文字的数量（Integer）；返回数据：一个一维字符串数组 String()，内容为随机中文简体字。这里支持最多不超过8个中文简体字输出，如需要更多输出请<a
     * href="http://www.webxml.com.cn/zh_cn/contact_us.aspx" target="_blank">联系我们</a>。
     */
    public java.lang.String[] getChineseFonts(int byFontsLength) throws java.rmi.RemoteException;

    /**
     * <br /><h3>获得随机英文、数字Web Services</h3><p>输入参数：byFontsLength =
     * 输出字的数量（Integer）；返回数据：一个一维字符串数组 String()，内容为随机英文、数字。为了避免混淆只输出，只随机产生
     * 1,2,3,4,5,6,7,8,9,A,C,D,E,F,H,K,L,M,N,P,R,S,T,W,X,Y,Z，这里支持最多不超过10个输出，如需要更多输出请<a
     * href="http://www.webxml.com.cn/zh_cn/contact_us.aspx" target="_blank">联系我们</a>。
     */
    public java.lang.String[] getCharFonts(int byFontsLength) throws java.rmi.RemoteException;
}
