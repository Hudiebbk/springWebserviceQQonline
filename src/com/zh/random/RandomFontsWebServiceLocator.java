/**
 * RandomFontsWebServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.zh.random;

public class RandomFontsWebServiceLocator extends org.apache.axis.client.Service implements com.zh.random.RandomFontsWebService {

/**
 * <a href="http://www.webxml.com.cn/" target="_blank">WebXml.com.cn</a>
 * <strong>随机英文、数字和中文简体字 WEB 服务</strong> 可用于验证码[<a href="http://www.webxml.com.cn/ValidateCode/ChineseValidateCode.aspx"
 * target="_blank">演示1</a>] [<a href="http://www.webxml.com.cn/ValidateCode/EnglishValidateCode.aspx"
 * target="_blank">演示2</a>]及其他方面，这里支持最多不超过8个随机中文简体字，10个随机英文、数字输出（一般也够了:P），如需要更多输出请<a
 * href="http://www.webxml.com.cn/zh_cn/contact_us.aspx" target="_blank">联系我们</a>，欢迎技术交流。
 * QQ：8409035<br /><strong>使用本站 WEB 服务请注明或链接本站：http://www.webxml.com.cn/
 * 感谢大家的支持</strong>！<br /><br />&nbsp;
 */

    public RandomFontsWebServiceLocator() {
    }


    public RandomFontsWebServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public RandomFontsWebServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for RandomFontsWebServiceSoap
    private java.lang.String RandomFontsWebServiceSoap_address = "http://ws.webxml.com.cn/WebServices/RandomFontsWebService.asmx";

    public java.lang.String getRandomFontsWebServiceSoapAddress() {
        return RandomFontsWebServiceSoap_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String RandomFontsWebServiceSoapWSDDServiceName = "RandomFontsWebServiceSoap";

    public java.lang.String getRandomFontsWebServiceSoapWSDDServiceName() {
        return RandomFontsWebServiceSoapWSDDServiceName;
    }

    public void setRandomFontsWebServiceSoapWSDDServiceName(java.lang.String name) {
        RandomFontsWebServiceSoapWSDDServiceName = name;
    }

    public com.zh.random.RandomFontsWebServiceSoap_PortType getRandomFontsWebServiceSoap() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(RandomFontsWebServiceSoap_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getRandomFontsWebServiceSoap(endpoint);
    }

    public com.zh.random.RandomFontsWebServiceSoap_PortType getRandomFontsWebServiceSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.zh.random.RandomFontsWebServiceSoap_BindingStub _stub = new com.zh.random.RandomFontsWebServiceSoap_BindingStub(portAddress, this);
            _stub.setPortName(getRandomFontsWebServiceSoapWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setRandomFontsWebServiceSoapEndpointAddress(java.lang.String address) {
        RandomFontsWebServiceSoap_address = address;
    }


    // Use to get a proxy class for RandomFontsWebServiceSoap12
    private java.lang.String RandomFontsWebServiceSoap12_address = "http://ws.webxml.com.cn/WebServices/RandomFontsWebService.asmx";

    public java.lang.String getRandomFontsWebServiceSoap12Address() {
        return RandomFontsWebServiceSoap12_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String RandomFontsWebServiceSoap12WSDDServiceName = "RandomFontsWebServiceSoap12";

    public java.lang.String getRandomFontsWebServiceSoap12WSDDServiceName() {
        return RandomFontsWebServiceSoap12WSDDServiceName;
    }

    public void setRandomFontsWebServiceSoap12WSDDServiceName(java.lang.String name) {
        RandomFontsWebServiceSoap12WSDDServiceName = name;
    }

    public com.zh.random.RandomFontsWebServiceSoap_PortType getRandomFontsWebServiceSoap12() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(RandomFontsWebServiceSoap12_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getRandomFontsWebServiceSoap12(endpoint);
    }

    public com.zh.random.RandomFontsWebServiceSoap_PortType getRandomFontsWebServiceSoap12(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.zh.random.RandomFontsWebServiceSoap12Stub _stub = new com.zh.random.RandomFontsWebServiceSoap12Stub(portAddress, this);
            _stub.setPortName(getRandomFontsWebServiceSoap12WSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setRandomFontsWebServiceSoap12EndpointAddress(java.lang.String address) {
        RandomFontsWebServiceSoap12_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     * This service has multiple ports for a given interface;
     * the proxy implementation returned may be indeterminate.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.zh.random.RandomFontsWebServiceSoap_PortType.class.isAssignableFrom(serviceEndpointInterface)) {
                com.zh.random.RandomFontsWebServiceSoap_BindingStub _stub = new com.zh.random.RandomFontsWebServiceSoap_BindingStub(new java.net.URL(RandomFontsWebServiceSoap_address), this);
                _stub.setPortName(getRandomFontsWebServiceSoapWSDDServiceName());
                return _stub;
            }
            if (com.zh.random.RandomFontsWebServiceSoap_PortType.class.isAssignableFrom(serviceEndpointInterface)) {
                com.zh.random.RandomFontsWebServiceSoap12Stub _stub = new com.zh.random.RandomFontsWebServiceSoap12Stub(new java.net.URL(RandomFontsWebServiceSoap12_address), this);
                _stub.setPortName(getRandomFontsWebServiceSoap12WSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("RandomFontsWebServiceSoap".equals(inputPortName)) {
            return getRandomFontsWebServiceSoap();
        }
        else if ("RandomFontsWebServiceSoap12".equals(inputPortName)) {
            return getRandomFontsWebServiceSoap12();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://WebXml.com.cn/", "RandomFontsWebService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://WebXml.com.cn/", "RandomFontsWebServiceSoap"));
            ports.add(new javax.xml.namespace.QName("http://WebXml.com.cn/", "RandomFontsWebServiceSoap12"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("RandomFontsWebServiceSoap".equals(portName)) {
            setRandomFontsWebServiceSoapEndpointAddress(address);
        }
        else 
if ("RandomFontsWebServiceSoap12".equals(portName)) {
            setRandomFontsWebServiceSoap12EndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
