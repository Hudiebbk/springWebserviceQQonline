/**
 * QqOnlineWebServiceTestCase.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.zh.wl;

public class QqOnlineWebServiceTestCase extends junit.framework.TestCase {
    public QqOnlineWebServiceTestCase(java.lang.String name) {
        super(name);
    }

    public void testqqOnlineWebServiceSoap12WSDL() throws Exception {
        javax.xml.rpc.ServiceFactory serviceFactory = javax.xml.rpc.ServiceFactory.newInstance();
        java.net.URL url = new java.net.URL(new com.zh.wl.QqOnlineWebServiceLocator().getqqOnlineWebServiceSoap12Address() + "?WSDL");
        javax.xml.rpc.Service service = serviceFactory.createService(url, new com.zh.wl.QqOnlineWebServiceLocator().getServiceName());
        assertTrue(service != null);
    }

    public void test1qqOnlineWebServiceSoap12QqCheckOnline() throws Exception {
        com.zh.wl.QqOnlineWebServiceSoap12Stub binding;
        try {
            binding = (com.zh.wl.QqOnlineWebServiceSoap12Stub)
                          new com.zh.wl.QqOnlineWebServiceLocator().getqqOnlineWebServiceSoap12();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        java.lang.String value = null;
        value = binding.qqCheckOnline(new java.lang.String());
        // TBD - validate results
    }

    public void testqqOnlineWebServiceSoapWSDL() throws Exception {
        javax.xml.rpc.ServiceFactory serviceFactory = javax.xml.rpc.ServiceFactory.newInstance();
        java.net.URL url = new java.net.URL(new com.zh.wl.QqOnlineWebServiceLocator().getqqOnlineWebServiceSoapAddress() + "?WSDL");
        javax.xml.rpc.Service service = serviceFactory.createService(url, new com.zh.wl.QqOnlineWebServiceLocator().getServiceName());
        assertTrue(service != null);
    }

    public void test2qqOnlineWebServiceSoapQqCheckOnline() throws Exception {
        com.zh.wl.QqOnlineWebServiceSoap_BindingStub binding;
        try {
            binding = (com.zh.wl.QqOnlineWebServiceSoap_BindingStub)
                          new com.zh.wl.QqOnlineWebServiceLocator().getqqOnlineWebServiceSoap();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        java.lang.String value = null;
        value = binding.qqCheckOnline(new java.lang.String());
        // TBD - validate results
    }

}
