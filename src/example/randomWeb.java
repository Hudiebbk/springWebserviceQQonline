package example;

import com.zh.random.RandomFontsWebServiceLocator;
import com.zh.random.RandomFontsWebServiceSoap_PortType;
import org.junit.Test;

import javax.xml.rpc.ServiceException;
import java.rmi.RemoteException;

public class randomWeb {

    public String[] randomWeb() throws ServiceException, RemoteException {
        RandomFontsWebServiceLocator locator = new RandomFontsWebServiceLocator();
        Integer num = 6;
        RandomFontsWebServiceSoap_PortType portType = locator.getRandomFontsWebServiceSoap();
        String[] strings = portType.getCharFonts(num);
        return strings;
    }


    public String Charset() throws ServiceException, RemoteException {
        String str = "";
        String[] strings1 = randomWeb();
        for (int i = 0; i < strings1.length; i++) {
            str = str.concat(strings1[i]);
        }
        System.out.println(str);
        return str;
    }

    @Test
    public void chartSet() throws ServiceException, RemoteException {
        String string = "ZP32XX";
        if (string.equals(Charset())) {
            System.out.println("验证成功");
        } else {
            System.out.println("验证失败");
        }
    }
}
