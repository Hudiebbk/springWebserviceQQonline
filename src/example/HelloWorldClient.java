package example;

import com.zh.wl.QqOnlineWebServiceLocator;
import com.zh.wl.QqOnlineWebServiceSoap_PortType;
import org.junit.Test;

import javax.xml.rpc.ServiceException;
import java.rmi.RemoteException;

public class HelloWorldClient {
    public void HelloWorldClient() {

    }

    @Test
    public void qqOnline() {
        String qq = null;
        QqOnlineWebServiceLocator locator = new QqOnlineWebServiceLocator();
        try {
            qq = "362329006";
            QqOnlineWebServiceSoap_PortType portType = locator.getqqOnlineWebServiceSoap();
            String qqOnline = portType.qqCheckOnline(qq);
            System.out.println(qqOnline);
            if (qqOnline.equals("Y")) {
                System.out.println(qq + "在线");
            } else {
                System.out.println(qq + "离线");
            }
        } catch (ServiceException e) {
            e.printStackTrace();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
}
