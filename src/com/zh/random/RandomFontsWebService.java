/**
 * RandomFontsWebService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.zh.random;

public interface RandomFontsWebService extends javax.xml.rpc.Service {

/**
 * <a href="http://www.webxml.com.cn/" target="_blank">WebXml.com.cn</a>
 * <strong>随机英文、数字和中文简体字 WEB 服务</strong> 可用于验证码[<a href="http://www.webxml.com.cn/ValidateCode/ChineseValidateCode.aspx"
 * target="_blank">演示1</a>] [<a href="http://www.webxml.com.cn/ValidateCode/EnglishValidateCode.aspx"
 * target="_blank">演示2</a>]及其他方面，这里支持最多不超过8个随机中文简体字，10个随机英文、数字输出（一般也够了:P），如需要更多输出请<a
 * href="http://www.webxml.com.cn/zh_cn/contact_us.aspx" target="_blank">联系我们</a>，欢迎技术交流。
 * QQ：8409035<br /><strong>使用本站 WEB 服务请注明或链接本站：http://www.webxml.com.cn/
 * 感谢大家的支持</strong>！<br /><br />&nbsp;
 */
    public java.lang.String getRandomFontsWebServiceSoapAddress();

    public com.zh.random.RandomFontsWebServiceSoap_PortType getRandomFontsWebServiceSoap() throws javax.xml.rpc.ServiceException;

    public com.zh.random.RandomFontsWebServiceSoap_PortType getRandomFontsWebServiceSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
    public java.lang.String getRandomFontsWebServiceSoap12Address();

    public com.zh.random.RandomFontsWebServiceSoap_PortType getRandomFontsWebServiceSoap12() throws javax.xml.rpc.ServiceException;

    public com.zh.random.RandomFontsWebServiceSoap_PortType getRandomFontsWebServiceSoap12(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
