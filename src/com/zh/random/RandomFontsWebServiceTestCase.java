/**
 * RandomFontsWebServiceTestCase.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.zh.random;

public class RandomFontsWebServiceTestCase extends junit.framework.TestCase {
    public RandomFontsWebServiceTestCase(java.lang.String name) {
        super(name);
    }

    public void testRandomFontsWebServiceSoapWSDL() throws Exception {
        javax.xml.rpc.ServiceFactory serviceFactory = javax.xml.rpc.ServiceFactory.newInstance();
        java.net.URL url = new java.net.URL(new com.zh.random.RandomFontsWebServiceLocator().getRandomFontsWebServiceSoapAddress() + "?WSDL");
        javax.xml.rpc.Service service = serviceFactory.createService(url, new com.zh.random.RandomFontsWebServiceLocator().getServiceName());
        assertTrue(service != null);
    }

    public void test1RandomFontsWebServiceSoapGetChineseFonts() throws Exception {
        com.zh.random.RandomFontsWebServiceSoap_BindingStub binding;
        try {
            binding = (com.zh.random.RandomFontsWebServiceSoap_BindingStub)
                          new com.zh.random.RandomFontsWebServiceLocator().getRandomFontsWebServiceSoap();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        java.lang.String[] value = null;
        value = binding.getChineseFonts(0);
        // TBD - validate results
    }

    public void test2RandomFontsWebServiceSoapGetCharFonts() throws Exception {
        com.zh.random.RandomFontsWebServiceSoap_BindingStub binding;
        try {
            binding = (com.zh.random.RandomFontsWebServiceSoap_BindingStub)
                          new com.zh.random.RandomFontsWebServiceLocator().getRandomFontsWebServiceSoap();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        java.lang.String[] value = null;
        value = binding.getCharFonts(0);
        // TBD - validate results
    }

    public void testRandomFontsWebServiceSoap12WSDL() throws Exception {
        javax.xml.rpc.ServiceFactory serviceFactory = javax.xml.rpc.ServiceFactory.newInstance();
        java.net.URL url = new java.net.URL(new com.zh.random.RandomFontsWebServiceLocator().getRandomFontsWebServiceSoap12Address() + "?WSDL");
        javax.xml.rpc.Service service = serviceFactory.createService(url, new com.zh.random.RandomFontsWebServiceLocator().getServiceName());
        assertTrue(service != null);
    }

    public void test3RandomFontsWebServiceSoap12GetChineseFonts() throws Exception {
        com.zh.random.RandomFontsWebServiceSoap12Stub binding;
        try {
            binding = (com.zh.random.RandomFontsWebServiceSoap12Stub)
                          new com.zh.random.RandomFontsWebServiceLocator().getRandomFontsWebServiceSoap12();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        java.lang.String[] value = null;
        value = binding.getChineseFonts(0);
        // TBD - validate results
    }

    public void test4RandomFontsWebServiceSoap12GetCharFonts() throws Exception {
        com.zh.random.RandomFontsWebServiceSoap12Stub binding;
        try {
            binding = (com.zh.random.RandomFontsWebServiceSoap12Stub)
                          new com.zh.random.RandomFontsWebServiceLocator().getRandomFontsWebServiceSoap12();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        java.lang.String[] value = null;
        value = binding.getCharFonts(0);
        // TBD - validate results
    }

}
